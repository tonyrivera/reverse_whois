# reverse_whois

### Installation
`pip install -r requirements.txt`

### Usage 
`reverse_whois.py -i foo@bar.com`

### Output
Domain Name  
afinitech.net  
arthurmeschian.com  
athedral.net  
erraticus.com  
goebelfamily.com  
hyperbolicspace.org  
legalytix.com  
mtviewioof.org  
obbtech.com  
siliconvalleyoddfellows.org  
spackle.today  
stikic.me  
stumblingmedia.us  
